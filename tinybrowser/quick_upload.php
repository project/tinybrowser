<?php
/*
 *  Quick Upload handler (Ajax upload handler)
 */
require_once('config_tinybrowser.php');
// Set language
if(isset($tinybrowser['language']) && file_exists('langs/'.$tinybrowser['language'].'.php')) {
	require_once('langs/'.$tinybrowser['language'].'.php'); 
}
else {
	require_once('langs/en.php'); // Falls back to English
}
require_once('fns_tinybrowser.php');

// Check session, if it exists
if(session_id() != '') {
	if(!isset($_SESSION[$tinybrowser['sessioncheck']])) {
		echo TB_DENIED;
		exit;
	}
}

$validtypes = array('image','media','file');
$typenow = ((isset($_REQUEST['type']) && in_array($_REQUEST['type'],$validtypes)) ? $_REQUEST['type'] : 'image');
$foldernow = str_replace(array('../','..\\','./','.\\'),'',(isset($_REQUEST['folder']) && !empty($_REQUEST['folder']) ? urldecode($_REQUEST['folder']) : ''));

// check upload permission 
if(!$tinybrowser['allowupload']) {
	echo "Error: uload operation is not permitted.";
	return;
}

$uploaddir = $tinybrowser['docroot'].$tinybrowser['path'][$typenow].$foldernow;
$uploadfile = $uploaddir . basename($_FILES['file']['name']);

$nameparts = explode('.', $_FILES['file']['name']);
$ext = end($nameparts);

if(!validateExtension($ext, $tinybrowser['prohibited'])) {
	echo "Error: this type of file is prohibited to be uploaded.";
	return;
}

// quota support with quick upload
if($tinybrowser['quota'][$typenow] > 0) {
	$dirsize = dirsize($tinybrowser['docroot'].$tinybrowser['path'][$typenow]);
	$remain_space = $tinybrowser['quota'][$typenow];
	$filesize = filesize($_FILES['file']['tmp_name']);
	if($remain_space < $filesize) {
		echo "Error: Allocated disk space is full.";
		return;
	}
}

/* DEBUG
print "\r\n";
print "typenow: " . $typenow. "\r\n";
print "foldernow: " . $foldernow. "\r\n";
print "uloadfile (source): " . $_FILES['file']['tmp_name'] . "\r\n";
print "uloadfile (destination): " . $uploadfile . "\r\n";
*/

if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
	echo "Success";
} 
else {
	// WARNING! DO NOT USE "FALSE" STRING AS A RESPONSE!
	// Otherwise onSubmit event will not be fired
	echo "Error: Moving uploaded file to the destination failed.";
}
